﻿using System.Reflection;

namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string path = "D:\\task2\\bin\\Debug\\net6.0\\task6.dll";
            Assembly assembly = null;
            try
            {
                assembly = Assembly.Load(path);
                Console.WriteLine("Opened successfully");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Type[] types = assembly.GetTypes();
            foreach (Type type in types)
            {
                Console.WriteLine("Type: " + type);
                var methods = type.GetMethods();
                if (methods != null)
                {
                    foreach (var method in methods)
                    {
                        string methodstr = "Method " + method.Name + "\n";
                        var methodBody = method.GetMethodBody();
                        if (methodBody != null)
                        {
                            var byteArr = methodBody.GetILAsByteArray();
                            foreach (var b in byteArr)
                            {
                                methodstr += b + ":";
                            }
                        }
                        Console.WriteLine(methodstr);
                    }
                }
            }
        }
    }
}