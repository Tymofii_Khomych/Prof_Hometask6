﻿using task2;
using System.Reflection;
using System.Reflection.Emit;

namespace task3
{
    internal class Program
    {
        static double ConvertToFarhenheit(Temperature temp)
        {
            Type t = temp.GetType();
            FieldInfo fi = t.GetField("temperature");
            double temperatureValue = (double)fi.GetValue(temp);

            double farhenheitValue = temperatureValue * 1.8 + 32;

            return farhenheitValue;
        }

        static void Main(string[] args)
        {
            Temperature temp = new Temperature(20);
            temp.Print();

            double farhenheitValue = ConvertToFarhenheit(temp);

            Console.WriteLine($"Temperature in Farhenheit: {farhenheitValue} F");
        }
    }
}