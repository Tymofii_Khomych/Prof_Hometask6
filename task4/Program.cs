﻿namespace task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Reflection in .NET
            // https://learn.microsoft.com/en-us/dotnet/framework/reflection-and-codedom/reflection

            /* 
             * The classes in the System.Reflection namespace, together with System.Type, enable you to obtain information about loaded 
             * assemblies and the types defined within them, such as classes, interfaces, and value types (that is, structures and enumerations). 
             * You can also use reflection to create type instances at run time, and to invoke and access them. For topics about specific aspects 
             * of reflection, see Related Topics at the end of this overview.
             * 
             * The common language runtime loader manages application domains, which constitute defined boundaries around objects that have the same 
             * application scope. This management includes loading each assembly into the appropriate application domain and controlling the memory 
             * layout of the type hierarchy within each assembly.
             * 
             * Assemblies contain modules, modules contain types, and types contain members. Reflection provides objects that encapsulate assemblies, 
             * modules, and types. You can use reflection to dynamically create an instance of a type, bind the type to an existing object, or get the 
             * type from an existing object. You can then invoke the type's methods or access its fields and properties. Typical uses of reflection 
             * include the following:
             * 
             * Use Assembly to define and load assemblies, load modules that are listed in the assembly manifest, and locate a type from this assembly and 
             * create an instance of it.

                Use Module to discover information such as the assembly that contains the module and the classes in the module. 
                You can also get all global methods or other specific, non-global methods defined on the module.

                Use ConstructorInfo to discover information such as the name, parameters, access modifiers (such as public or private), 
                and implementation details (such as abstract or virtual) of a constructor. Use the GetConstructors or GetConstructor method of a 
                Type to invoke a specific constructor.

                Use MethodInfo to discover information such as the name, return type, parameters, access modifiers (such as public or private), 
                and implementation details (such as abstract or virtual) of a method. Use the GetMethods or GetMethod method of a Type to invoke a 
                specific method.

                Use FieldInfo to discover information such as the name, access modifiers (such as public or private) and implementation details 
                (such as static) of a field, and to get or set field values.

                Use EventInfo to discover information such as the name, event-handler data type, custom attributes, declaring type, and reflected 
                type of an event, and to add or remove event handlers.

                Use PropertyInfo to discover information such as the name, data type, declaring type, reflected type, and read-only or writable 
                status of a property, and to get or set property values.

                Use ParameterInfo to discover information such as a parameter's name, data type, whether a parameter is an input or output parameter, 
                and the position of the parameter in a method signature.

                Use CustomAttributeData to discover information about custom attributes when you are working in the reflection-only context of an 
                application domain. CustomAttributeData allows you to examine attributes without creating instances of them. The classes of the 
                System.Reflection.Emit namespace provide a specialized form of reflection that enables you to build types at run time.


             * Reflection can also be used to create applications called type browsers, which enable users to select types and then view the 
             * information about those types.
             *
             * There are other uses for reflection. Compilers for languages such as JScript use reflection to construct symbol tables. The classes in 
             * the System.Runtime.Serialization namespace use reflection to access data and to determine which fields to persist. The classes in the 
             * System.Runtime.Remoting namespace use reflection indirectly through serialization.
             */
        }
    }
}