﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    public class Temperature
    {
        public double temperature;

        public double myProp
        {
            get { return temperature;}
            set { temperature = value; }
        }

        public Temperature() { }
        public Temperature(double temperature)
        {
            this.temperature = temperature;
        }

        public void Print()
        {
            Console.WriteLine($"Current temperature = {temperature} C");
        }
    }
}
